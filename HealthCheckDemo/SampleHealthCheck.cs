using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace HealthCheckDemo;

public class SampleHealthCheck : IHealthCheck
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
    {
        var someNumber = new Random().Next(1, 10);
        
        return Task.FromResult(HealthCheckResult.Healthy("I am good"));
        // if (someNumber % 2 == 0)
        // {
        //     return Task.FromResult(HealthCheckResult.Healthy("I am good"));
        // }
        //
        // return Task.FromResult(new HealthCheckResult(context.Registration.FailureStatus, "I am bad"));
    }
}