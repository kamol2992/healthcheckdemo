using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HealthCheckDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly ILogger<NotificationController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public NotificationController(ILogger<NotificationController> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }
        
        [HttpPost("send")]
        public async Task<string> Get([FromBody] NotificationSendRequest request)
        {
            //https://api.telegram.org/bot5698832396:AAESr9o6H4TjfidU5uUvdt6IZGchdLAytjE/sendMessage?chat_id=@channel2992&text=[[LIVENESS]] [[FAILURE]]

            var token = "5698832396:AAESr9o6H4TjfidU5uUvdt6IZGchdLAytjE";
            var chatId = "@channel2992";
            var text = request.Message;
            
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get,
                $"https://api.telegram.org/bot{token}/sendMessage?chat_id={chatId}&text={text}");

            var httpClient = _httpClientFactory.CreateClient();
            var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
            
            
            _logger.LogInformation(JsonConvert.SerializeObject(request));
            
            return httpResponseMessage.StatusCode.ToString();
        }
    }

    public class NotificationSendRequest
    {
        //public string HealthCheck { get; set; }
        public string Message { get; set; }
        //public RestoredPayload RestoredPayload { get; set; }
    }

    public class Payload
    {
        public string HealthCheck { get; set; }
        public string Message { get; set; }
    }

    public class RestoredPayload
    {
        public string Message { get; set; }
    }
}
