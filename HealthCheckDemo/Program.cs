using HealthCheckDemo;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddHttpClient();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHealthChecks()
    //.AddCheck<SampleHealthCheck>("Sample");
    //.AddNpgSql(npgsqlConnectionString:builder.Configuration.GetConnectionString("DefaultConnection"))
    //.AddRabbitMQ(rabbitConnectionString: "amqps://guest:guest@localhost/my_vhost")
    //.AddRabbitMQ(rabbitConnectionString: "amqp://guest:guest@localhost:5672/")
    // //.AddRabbitMQ(rabbitConnectionString: "amqps://guest:guest@host2/vhost")
    .AddCheck<SampleHealthCheck>(
        "Sample",
        failureStatus:HealthStatus.Degraded,
        tags: new[] { "sample" }
        );

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapHealthChecks("/health", new HealthCheckOptions()
{
    //Predicate = registration => registration.Tags.Contains("sample"),
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
});
//.RequireHost(new[] { "localhost:7111" });
//.RequireCors("SomePolicyName")
//.RequireAuthorization()

app.Run();